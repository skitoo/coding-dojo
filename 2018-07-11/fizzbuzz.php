<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jeu d'enfant</title>
    <style>
        .numbers {
            display: flex;
            flex-wrap: wrap;
            padding: 0;
            margin: 0;
        }
        .numbers li {
            box-sizing: border-box;
            margin: 10px;
            border: 1px solid #333;
            padding: 10px;
            width: calc(25% - 20px);
            list-style: none;
            text-align: center;
            border-radius: 3px;
        }
        .fizz {
            background: rgba(255, 0, 0, .2);
        }
        .buzz {
            background: rgba(0, 255, 0, .2);
        }
        .fizz.buzz {
            background: rgba(0, 0, 255, .2);
        }

        @media (max-width: 480px) {
            .numbers li {
                width: calc(50% - 20px);
            }
        }
    </style>
</head>
<body>

<?php

// Boucle pour verifier si les nombres sont divisibles
// par 3 ou 5 afin d'afficher Fizz pour les divisibles de 3
// et Buzz pour les divisibles par 5 et FizzBuzz pour les 2
echo '<ul class="numbers">';

$keys = [
    3 => 'Fizz',
    5 => 'Buzz',
];
for ($i = 1; $i < 101; $i++) {
    $text = '';
    $class = '';
    foreach ($keys as $number => $say) {
        if ((strpos($i, (string)$number) !== false) || !($i % $number)) {
            $text .= $say;
            $class .= strtolower($say) . ' ';
        }
    }
    echo '<li class="'.rtrim($class).'">'.($text == '' ? $i : $text).'</li>';
}
echo '</ul>';
?>
</body>
</html>