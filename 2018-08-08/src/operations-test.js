import { add, substract, multiply, divide } from './operations';


describe('Operation', () => {
  describe('add', () => {
    it('should add 2 values', () => {
      expect(add(2, 2)).to.be.equal(4);
      expect(add(2, -4)).to.be.equal(-2);
    });
  });
  describe('substract', () => {
    it('should substract 2 values', () => {
      expect(substract(2, 2)).to.be.equal(0);
      expect(substract(-2, 20)).to.be.equal(-22);
    });
  });
  describe('multiply', () => {
    it('should exists', () => {
      expect(multiply).to.be.a('function');
    });
    it('should multiply 2 values', () => {
      expect(multiply(4, 3)).to.be.equal(12);
      expect(multiply(5, 0)).to.be.equal(0);
    });
  });
  describe('divide', () => {
    it('should exists', () => {
      expect(divide).to.be.a('function');
    });
    it('should divide 2 values', () => {
      expect(divide(10, 2)).to.be.equal(5);
      expect(divide(12, 4)).to.be.equal(3);
    });
  });
});
