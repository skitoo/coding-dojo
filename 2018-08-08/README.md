# Coding dojo du 08-08-2018

## Installation de l'environnement

```bash
$ yarn install
```

## Lancement des tests

```bash
$ yarn test
```

## Build de l'application

```bash
$ yarn watch
```
